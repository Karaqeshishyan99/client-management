# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

-   In this project you can manage client data
-   Version 1.0.0

### How do I get set up?

-   Clone this repository to create a local copy on your computer

```
cd client-management/api/
```

-   Open api folder

```
npm install
```

-   Install node modules

```
npm run start:dev or npm run start:prod
```

-   Runs the app in development or production mode.
-   You can see and test all api endpoints by opening http://localhost:4000/api-doc/ in your browser

### Who do I talk to?

-   Repo is created by Khachik Karakeshishyan
