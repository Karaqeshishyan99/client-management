const mongoose = require("mongoose");
const { db } = require("./constants");

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
};

// Database connection
mongoose.connect(db.MONGO_CONN_STRING, options).then(
    () => {
        console.log("Database connection established");
    },
    (err) => {
        console.log(`Error connecting Database instance due to: ${err}`);
    }
);
