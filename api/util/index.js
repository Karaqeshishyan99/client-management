// Check if the fields are undefined
const isUndefined = () => {
    for (let i = 0; i < arguments.length; i++) {
        if (arguments[i] == undefined) {
            return true;
        }
    }
    return false;
};

// Send Error message
const sendError = (res, statusCode, message) => {
    res.status(statusCode).json({
        message,
    });
};

// Send response to the client
const sendResponse = (res, statusCode, message, data) => {
    res.status(statusCode).json({
        message,
        data,
    });
};

module.exports = {
    isUndefined,
    sendError,
    sendResponse,
};
