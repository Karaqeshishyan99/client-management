const express = require("express");
const router = express.Router();
const clientRoutes = require("./client/routes");
const providerRoutes = require("./provider/routes");

router.use("/clients", clientRoutes);
router.use("/providers", providerRoutes);

module.exports = router;
