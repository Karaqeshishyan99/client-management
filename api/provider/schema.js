const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Model = mongoose.model;
const ObjectId = mongoose.Types.ObjectId;

const ProviderSchema = new Schema({
    name: { type: String, required: true },
});

module.exports = Model("Provider", ProviderSchema);
