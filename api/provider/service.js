const model = require("./schema");

const getAll = async () => {
    const providers = await model.find({});
    return providers;
};

const create = async (providerDto) => {
    const provider = await model.create(providerDto);
    return provider;
};

const update = async (id, providerDto) => {
    const provider = await model.findById(id);
    provider.name = providerDto.name;
    return provider.save();
};

const remove = async (id) => {
    const provider = await model.findOneAndDelete({ _id: id });
    return provider;
};

module.exports = {
    getAll,
    create,
    update,
    remove,
};
