const express = require("express");
const app = express();
const cors = require("cors");
const morgan = require("morgan");
const db = require("./db");
const routes = require("./routes");
const port = process.env.port || 4000;
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

// Adding middlewares
app.use(express.json());
app.use(cors());
app.use(morgan("dev"));
app.use("/api", routes);
app.use("/api-doc", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use((req, res, next) => {
    const err = new Error("Not found");
    err.status = 404;
    next(err);
});
app.use((err, req, res, next) => {
    let status = err.status || 500;
    res.status(status);
    res.json({
        message: err.message || err,
    });
});

// Server listening
app.listen(port, () => {
    console.log(`Express server listening on prot ${port}`);
});

module.exports = app;
