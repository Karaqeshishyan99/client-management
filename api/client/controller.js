const service = require("./service");
const { isUndefined, sendError, sendResponse } = require("../util");
const { messages, statuses } = require("../constants");

const getAll = async (req, res) => {
    try {
        let data = await service.getAll();
        sendResponse(res, statuses.SUCCESS, null, data);
    } catch (err) {
        sendError(res, statuses.INTERNAL_ERROR, err.message);
    }
};

const getOne = async (req, res) => {
    const id = req.params.id;
    if (isUndefined(id)) {
        return sendError(res, statuses.BAD_REQUEST, messages.INVALID_PARAMS);
    }
    try {
        let data = await service.getOne(id);
        sendResponse(res, statuses.SUCCESS, null, data);
    } catch (err) {
        sendError(res, statuses.INTERNAL_ERROR, err.message);
    }
};

const create = async (req, res) => {
    const { name, email, phone } = req.body;
    if (isUndefined(name, email, phone)) {
        return sendError(res, statuses.BAD_REQUEST, messages.ERROR_INVALID_PARAMS);
    }
    try {
        let data = await service.create(req.body);
        sendResponse(res, statuses.SUCCESS, messages.CLIENT_CREATED, data);
    } catch (err) {
        sendError(res, statuses.INTERNAL_ERROR, err.message);
    }
};

const update = async (req, res) => {
    const id = req.params.id;
    let { name, email, phone } = req.body;
    if (isUndefined(id, name, email, phone)) {
        return sendError(res, statuses.BAD_REQUEST, messages.ERROR_INVALID_PARAMS);
    }

    try {
        let data = await service.update(id, req.body);
        sendResponse(res, statuses.SUCCESS, messages.CLIENT_UPDATED, data);
    } catch (err) {
        sendError(res, statuses.INTERNAL_ERROR, err.message);
    }
};

const remove = async (req, res) => {
    const id = req.params.id;
    if (isUndefined(id)) {
        return sendError(res, statuses.BAD_REQUEST, messages.ERROR_INVALID_PARAMS);
    }
    try {
        await service.remove(id);
        sendResponse(res, statuses.SUCCESS, messages.CLIENT_DELETED, id);
    } catch (err) {
        sendError(res, statuses.INTERNAL_ERROR, err.message);
    }
};

module.exports = {
    getAll,
    getOne,
    create,
    update,
    remove,
};
