const model = require("./schema");

const getAll = async () => {
    const clients = await model.find({}).populate("providers");
    return clients;
};

const getOne = async (id) => {
    const client = await model.findById(id);
    return client;
};

const create = async (clientDto) => {
    const client = await model.create(clientDto);
    return client;
};

const update = async (id, clientDto) => {
    const client = await model.findById(id);
    client.name = clientDto.name;
    client.email = clientDto.email;
    client.phone = clientDto.phone;
    client.providers = clientDto.providers;
    return client.save();
};

const remove = async (id) => {
    const client = await model.findOneAndDelete({ _id: id });
    return client;
};

module.exports = {
    getAll,
    getOne,
    create,
    update,
    remove,
};
