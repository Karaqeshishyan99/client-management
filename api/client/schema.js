const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Model = mongoose.model;
const ObjectId = mongoose.Types.ObjectId;

const clientSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    phone: { type: String, required: true },
    providers: [{ type: ObjectId, ref: "Provider" }],
});

clientSchema.post("save", (error, doc, next) => {
    if (error.name === "MongoError" && error.code === 11000) {
        next(new Error("This email already exist"));
    } else {
        next();
    }
});

module.exports = Model("Client", clientSchema);
