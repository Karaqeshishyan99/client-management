const db = {
    MONGO_CONN_STRING: "mongodb+srv://Khachik:testPassword@cluster0.okqpi.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
};

const messages = {
    // Client
    CLIENT_CREATED: "New client is created",
    CLIENT_UPDATED: "The client was successfully updated",
    CLIENT_DELETED: "The client was successfully deleted",
    // Provider
    PROVIDER_CREATED: "New provider is created",
    PROVIDER_UPDATED: "The provider was successfully updated",
    PROVIDER_DELETED: "The provider was successfully deleted",
    // Error
    ERROR_INVALID_PARAMS: "Invalid Parameters",
    ERROR_NOT_FOUND: "The resourece was not found",
};

const statuses = {
    SUCCESS: 200,
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
    INTERNAL_ERROR: 500,
};

module.exports = {
    db,
    messages,
    statuses,
};
